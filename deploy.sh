docker run -d --label "traefik.http.routers.t$CI_JOB_ID.rule=Host(\`$CI_JOB_ID.dev.wdh.hu\`)" --network webconfhu_default gitlab-runner/$CI_JOB_ID

echo "http://$CI_JOB_ID.dev.wdh.hu"
